﻿namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.buttonDivision = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.buttonVezes = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.buttonMenos = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.buttonPonto = new System.Windows.Forms.Button();
            this.buttonMais = new System.Windows.Forms.Button();
            this.buttonIgual = new System.Windows.Forms.Button();
            this.buttonMemoClear = new System.Windows.Forms.Button();
            this.buttonMemoRecover = new System.Windows.Forms.Button();
            this.buttonPotencia = new System.Windows.Forms.Button();
            this.buttonMemoAdd = new System.Windows.Forms.Button();
            this.buttonSqrt = new System.Windows.Forms.Button();
            this.buttonMemoDel = new System.Windows.Forms.Button();
            this.buttonPercent = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonErase = new System.Windows.Forms.Button();
            this.buttonBackspace = new System.Windows.Forms.Button();
            this.buttonToggle = new System.Windows.Forms.Button();
            this.textBoxExpression = new System.Windows.Forms.TextBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn7.Enabled = false;
            this.btn7.FlatAppearance.BorderSize = 0;
            this.btn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn7.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(9, 164);
            this.btn7.Margin = new System.Windows.Forms.Padding(0);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(50, 50);
            this.btn7.TabIndex = 0;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.addText);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn8.Enabled = false;
            this.btn8.FlatAppearance.BorderSize = 0;
            this.btn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn8.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(59, 164);
            this.btn8.Margin = new System.Windows.Forms.Padding(0);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(50, 50);
            this.btn8.TabIndex = 1;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.addText);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn9.Enabled = false;
            this.btn9.FlatAppearance.BorderSize = 0;
            this.btn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(109, 164);
            this.btn9.Margin = new System.Windows.Forms.Padding(0);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(50, 50);
            this.btn9.TabIndex = 2;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.addText);
            // 
            // buttonDivision
            // 
            this.buttonDivision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonDivision.Enabled = false;
            this.buttonDivision.FlatAppearance.BorderSize = 0;
            this.buttonDivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDivision.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDivision.ForeColor = System.Drawing.Color.White;
            this.buttonDivision.Location = new System.Drawing.Point(159, 114);
            this.buttonDivision.Margin = new System.Windows.Forms.Padding(0);
            this.buttonDivision.Name = "buttonDivision";
            this.buttonDivision.Size = new System.Drawing.Size(50, 50);
            this.buttonDivision.TabIndex = 3;
            this.buttonDivision.Text = "÷";
            this.buttonDivision.UseVisualStyleBackColor = false;
            this.buttonDivision.Click += new System.EventHandler(this.addOperation);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn4.Enabled = false;
            this.btn4.FlatAppearance.BorderSize = 0;
            this.btn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn4.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(9, 214);
            this.btn4.Margin = new System.Windows.Forms.Padding(0);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(50, 50);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.addText);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn5.Enabled = false;
            this.btn5.FlatAppearance.BorderSize = 0;
            this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(59, 214);
            this.btn5.Margin = new System.Windows.Forms.Padding(0);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(50, 50);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.addText);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn6.Enabled = false;
            this.btn6.FlatAppearance.BorderSize = 0;
            this.btn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(109, 214);
            this.btn6.Margin = new System.Windows.Forms.Padding(0);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(50, 50);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.addText);
            // 
            // buttonVezes
            // 
            this.buttonVezes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonVezes.Enabled = false;
            this.buttonVezes.FlatAppearance.BorderSize = 0;
            this.buttonVezes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonVezes.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVezes.ForeColor = System.Drawing.Color.White;
            this.buttonVezes.Location = new System.Drawing.Point(159, 164);
            this.buttonVezes.Margin = new System.Windows.Forms.Padding(0);
            this.buttonVezes.Name = "buttonVezes";
            this.buttonVezes.Size = new System.Drawing.Size(50, 50);
            this.buttonVezes.TabIndex = 7;
            this.buttonVezes.Text = "x";
            this.buttonVezes.UseVisualStyleBackColor = false;
            this.buttonVezes.Click += new System.EventHandler(this.addOperation);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn1.Enabled = false;
            this.btn1.FlatAppearance.BorderSize = 0;
            this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(9, 264);
            this.btn1.Margin = new System.Windows.Forms.Padding(0);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(50, 50);
            this.btn1.TabIndex = 8;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.addText);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn2.Enabled = false;
            this.btn2.FlatAppearance.BorderSize = 0;
            this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(59, 264);
            this.btn2.Margin = new System.Windows.Forms.Padding(0);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(50, 50);
            this.btn2.TabIndex = 9;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.addText);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn3.Enabled = false;
            this.btn3.FlatAppearance.BorderSize = 0;
            this.btn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(109, 264);
            this.btn3.Margin = new System.Windows.Forms.Padding(0);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(50, 50);
            this.btn3.TabIndex = 10;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.addText);
            // 
            // buttonMenos
            // 
            this.buttonMenos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonMenos.Enabled = false;
            this.buttonMenos.FlatAppearance.BorderSize = 0;
            this.buttonMenos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMenos.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMenos.ForeColor = System.Drawing.Color.White;
            this.buttonMenos.Location = new System.Drawing.Point(159, 214);
            this.buttonMenos.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMenos.Name = "buttonMenos";
            this.buttonMenos.Size = new System.Drawing.Size(50, 50);
            this.buttonMenos.TabIndex = 11;
            this.buttonMenos.Text = "-";
            this.buttonMenos.UseVisualStyleBackColor = false;
            this.buttonMenos.Click += new System.EventHandler(this.addOperation);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.btn0.Enabled = false;
            this.btn0.FlatAppearance.BorderSize = 0;
            this.btn0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn0.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(9, 314);
            this.btn0.Margin = new System.Windows.Forms.Padding(0);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(150, 50);
            this.btn0.TabIndex = 12;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.addText);
            // 
            // buttonPonto
            // 
            this.buttonPonto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(195)))), ((int)(((byte)(199)))));
            this.buttonPonto.Enabled = false;
            this.buttonPonto.FlatAppearance.BorderSize = 0;
            this.buttonPonto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPonto.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPonto.Location = new System.Drawing.Point(159, 314);
            this.buttonPonto.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPonto.Name = "buttonPonto";
            this.buttonPonto.Size = new System.Drawing.Size(50, 50);
            this.buttonPonto.TabIndex = 13;
            this.buttonPonto.Text = ",";
            this.buttonPonto.UseVisualStyleBackColor = false;
            this.buttonPonto.Click += new System.EventHandler(this.addText);
            // 
            // buttonMais
            // 
            this.buttonMais.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonMais.Enabled = false;
            this.buttonMais.FlatAppearance.BorderSize = 0;
            this.buttonMais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMais.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMais.ForeColor = System.Drawing.Color.White;
            this.buttonMais.Location = new System.Drawing.Point(159, 264);
            this.buttonMais.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMais.Name = "buttonMais";
            this.buttonMais.Size = new System.Drawing.Size(50, 50);
            this.buttonMais.TabIndex = 14;
            this.buttonMais.Text = "+";
            this.buttonMais.UseVisualStyleBackColor = false;
            this.buttonMais.Click += new System.EventHandler(this.addOperation);
            // 
            // buttonIgual
            // 
            this.buttonIgual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.buttonIgual.Enabled = false;
            this.buttonIgual.FlatAppearance.BorderSize = 0;
            this.buttonIgual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonIgual.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonIgual.ForeColor = System.Drawing.Color.White;
            this.buttonIgual.Location = new System.Drawing.Point(209, 264);
            this.buttonIgual.Margin = new System.Windows.Forms.Padding(0);
            this.buttonIgual.Name = "buttonIgual";
            this.buttonIgual.Size = new System.Drawing.Size(50, 100);
            this.buttonIgual.TabIndex = 15;
            this.buttonIgual.Text = "=";
            this.buttonIgual.UseVisualStyleBackColor = false;
            this.buttonIgual.Click += new System.EventHandler(this.expressionCompute);
            // 
            // buttonMemoClear
            // 
            this.buttonMemoClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(112)))), ((int)(((byte)(143)))));
            this.buttonMemoClear.Enabled = false;
            this.buttonMemoClear.FlatAppearance.BorderSize = 0;
            this.buttonMemoClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMemoClear.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemoClear.ForeColor = System.Drawing.Color.White;
            this.buttonMemoClear.Location = new System.Drawing.Point(159, 64);
            this.buttonMemoClear.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMemoClear.Name = "buttonMemoClear";
            this.buttonMemoClear.Size = new System.Drawing.Size(50, 50);
            this.buttonMemoClear.TabIndex = 16;
            this.buttonMemoClear.Text = "MC";
            this.buttonMemoClear.UseVisualStyleBackColor = false;
            this.buttonMemoClear.Click += new System.EventHandler(this.memoClear);
            // 
            // buttonMemoRecover
            // 
            this.buttonMemoRecover.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(112)))), ((int)(((byte)(143)))));
            this.buttonMemoRecover.Enabled = false;
            this.buttonMemoRecover.FlatAppearance.BorderSize = 0;
            this.buttonMemoRecover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMemoRecover.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemoRecover.ForeColor = System.Drawing.Color.White;
            this.buttonMemoRecover.Location = new System.Drawing.Point(109, 64);
            this.buttonMemoRecover.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMemoRecover.Name = "buttonMemoRecover";
            this.buttonMemoRecover.Size = new System.Drawing.Size(50, 50);
            this.buttonMemoRecover.TabIndex = 17;
            this.buttonMemoRecover.Text = "MR";
            this.buttonMemoRecover.UseVisualStyleBackColor = false;
            this.buttonMemoRecover.Click += new System.EventHandler(this.memoReturn);
            // 
            // buttonPotencia
            // 
            this.buttonPotencia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonPotencia.Enabled = false;
            this.buttonPotencia.FlatAppearance.BorderSize = 0;
            this.buttonPotencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPotencia.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPotencia.ForeColor = System.Drawing.Color.White;
            this.buttonPotencia.Location = new System.Drawing.Point(109, 114);
            this.buttonPotencia.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPotencia.Name = "buttonPotencia";
            this.buttonPotencia.Size = new System.Drawing.Size(50, 50);
            this.buttonPotencia.TabIndex = 18;
            this.buttonPotencia.Text = "y˟";
            this.buttonPotencia.UseVisualStyleBackColor = false;
            this.buttonPotencia.Click += new System.EventHandler(this.addOperation);
            // 
            // buttonMemoAdd
            // 
            this.buttonMemoAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(112)))), ((int)(((byte)(143)))));
            this.buttonMemoAdd.Enabled = false;
            this.buttonMemoAdd.FlatAppearance.BorderSize = 0;
            this.buttonMemoAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMemoAdd.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemoAdd.ForeColor = System.Drawing.Color.White;
            this.buttonMemoAdd.Location = new System.Drawing.Point(59, 64);
            this.buttonMemoAdd.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMemoAdd.Name = "buttonMemoAdd";
            this.buttonMemoAdd.Size = new System.Drawing.Size(50, 50);
            this.buttonMemoAdd.TabIndex = 19;
            this.buttonMemoAdd.Text = "M+";
            this.buttonMemoAdd.UseVisualStyleBackColor = false;
            this.buttonMemoAdd.Click += new System.EventHandler(this.memoAdd);
            // 
            // buttonSqrt
            // 
            this.buttonSqrt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonSqrt.Enabled = false;
            this.buttonSqrt.FlatAppearance.BorderSize = 0;
            this.buttonSqrt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSqrt.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSqrt.ForeColor = System.Drawing.Color.White;
            this.buttonSqrt.Location = new System.Drawing.Point(59, 114);
            this.buttonSqrt.Margin = new System.Windows.Forms.Padding(0);
            this.buttonSqrt.Name = "buttonSqrt";
            this.buttonSqrt.Size = new System.Drawing.Size(50, 50);
            this.buttonSqrt.TabIndex = 20;
            this.buttonSqrt.Text = "√";
            this.buttonSqrt.UseVisualStyleBackColor = false;
            this.buttonSqrt.Click += new System.EventHandler(this.addOperation);
            // 
            // buttonMemoDel
            // 
            this.buttonMemoDel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(112)))), ((int)(((byte)(143)))));
            this.buttonMemoDel.Enabled = false;
            this.buttonMemoDel.FlatAppearance.BorderSize = 0;
            this.buttonMemoDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMemoDel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMemoDel.ForeColor = System.Drawing.Color.White;
            this.buttonMemoDel.Location = new System.Drawing.Point(9, 64);
            this.buttonMemoDel.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMemoDel.Name = "buttonMemoDel";
            this.buttonMemoDel.Size = new System.Drawing.Size(50, 50);
            this.buttonMemoDel.TabIndex = 21;
            this.buttonMemoDel.Text = "M-";
            this.buttonMemoDel.UseVisualStyleBackColor = false;
            this.buttonMemoDel.Click += new System.EventHandler(this.memoSub);
            // 
            // buttonPercent
            // 
            this.buttonPercent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.buttonPercent.Enabled = false;
            this.buttonPercent.FlatAppearance.BorderSize = 0;
            this.buttonPercent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPercent.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPercent.ForeColor = System.Drawing.Color.White;
            this.buttonPercent.Location = new System.Drawing.Point(9, 114);
            this.buttonPercent.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPercent.Name = "buttonPercent";
            this.buttonPercent.Size = new System.Drawing.Size(50, 50);
            this.buttonPercent.TabIndex = 22;
            this.buttonPercent.Text = "%";
            this.buttonPercent.UseVisualStyleBackColor = false;
            this.buttonPercent.Click += new System.EventHandler(this.addPercentage);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(140)))), ((int)(((byte)(141)))));
            this.buttonClear.Enabled = false;
            this.buttonClear.FlatAppearance.BorderSize = 0;
            this.buttonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClear.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClear.ForeColor = System.Drawing.Color.White;
            this.buttonClear.Location = new System.Drawing.Point(209, 214);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(50, 50);
            this.buttonClear.TabIndex = 23;
            this.buttonClear.Text = "C";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.clearText);
            // 
            // buttonErase
            // 
            this.buttonErase.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(140)))), ((int)(((byte)(141)))));
            this.buttonErase.Enabled = false;
            this.buttonErase.FlatAppearance.BorderSize = 0;
            this.buttonErase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonErase.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonErase.ForeColor = System.Drawing.Color.White;
            this.buttonErase.Location = new System.Drawing.Point(209, 164);
            this.buttonErase.Margin = new System.Windows.Forms.Padding(0);
            this.buttonErase.Name = "buttonErase";
            this.buttonErase.Size = new System.Drawing.Size(50, 50);
            this.buttonErase.TabIndex = 24;
            this.buttonErase.Text = "CE";
            this.buttonErase.UseVisualStyleBackColor = false;
            this.buttonErase.Click += new System.EventHandler(this.clearLastNumber);
            // 
            // buttonBackspace
            // 
            this.buttonBackspace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(140)))), ((int)(((byte)(141)))));
            this.buttonBackspace.Enabled = false;
            this.buttonBackspace.FlatAppearance.BorderSize = 0;
            this.buttonBackspace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackspace.Font = new System.Drawing.Font("Arial", 18F);
            this.buttonBackspace.ForeColor = System.Drawing.Color.White;
            this.buttonBackspace.Location = new System.Drawing.Point(209, 114);
            this.buttonBackspace.Margin = new System.Windows.Forms.Padding(0);
            this.buttonBackspace.Name = "buttonBackspace";
            this.buttonBackspace.Size = new System.Drawing.Size(50, 50);
            this.buttonBackspace.TabIndex = 25;
            this.buttonBackspace.Text = "←";
            this.buttonBackspace.UseVisualStyleBackColor = false;
            this.buttonBackspace.Click += new System.EventHandler(this.backspace);
            // 
            // buttonToggle
            // 
            this.buttonToggle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(174)))), ((int)(((byte)(96)))));
            this.buttonToggle.FlatAppearance.BorderSize = 0;
            this.buttonToggle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonToggle.Font = new System.Drawing.Font("Arial", 14.25F);
            this.buttonToggle.ForeColor = System.Drawing.Color.White;
            this.buttonToggle.Location = new System.Drawing.Point(209, 64);
            this.buttonToggle.Margin = new System.Windows.Forms.Padding(0);
            this.buttonToggle.Name = "buttonToggle";
            this.buttonToggle.Size = new System.Drawing.Size(50, 50);
            this.buttonToggle.TabIndex = 26;
            this.buttonToggle.Text = "On";
            this.buttonToggle.UseVisualStyleBackColor = false;
            this.buttonToggle.Click += new System.EventHandler(this.toggleStatus);
            // 
            // textBoxExpression
            // 
            this.textBoxExpression.BackColor = System.Drawing.Color.White;
            this.textBoxExpression.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxExpression.Font = new System.Drawing.Font("Arial", 15F);
            this.textBoxExpression.Location = new System.Drawing.Point(9, 12);
            this.textBoxExpression.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxExpression.Name = "textBoxExpression";
            this.textBoxExpression.ReadOnly = true;
            this.textBoxExpression.Size = new System.Drawing.Size(250, 23);
            this.textBoxExpression.TabIndex = 28;
            this.textBoxExpression.Text = "1 - 1";
            this.textBoxExpression.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxExpression.MouseEnter += new System.EventHandler(this.hoverTextBoxStatus);
            this.textBoxExpression.MouseLeave += new System.EventHandler(this.leaveTextBoxStatus);
            // 
            // textBoxResult
            // 
            this.textBoxResult.BackColor = System.Drawing.Color.White;
            this.textBoxResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResult.Font = new System.Drawing.Font("Arial", 15F);
            this.textBoxResult.Location = new System.Drawing.Point(9, 35);
            this.textBoxResult.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(250, 23);
            this.textBoxResult.TabIndex = 27;
            this.textBoxResult.Text = "0";
            this.textBoxResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxResult.MouseEnter += new System.EventHandler(this.hoverTextBoxStatus);
            this.textBoxResult.MouseLeave += new System.EventHandler(this.leaveTextBoxStatus);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.ClientSize = new System.Drawing.Size(268, 373);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.textBoxExpression);
            this.Controls.Add(this.buttonToggle);
            this.Controls.Add(this.buttonBackspace);
            this.Controls.Add(this.buttonErase);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonPercent);
            this.Controls.Add(this.buttonMemoDel);
            this.Controls.Add(this.buttonSqrt);
            this.Controls.Add(this.buttonMemoAdd);
            this.Controls.Add(this.buttonPotencia);
            this.Controls.Add(this.buttonMemoRecover);
            this.Controls.Add(this.buttonMemoClear);
            this.Controls.Add(this.buttonIgual);
            this.Controls.Add(this.buttonMais);
            this.Controls.Add(this.buttonPonto);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.buttonMenos);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.buttonVezes);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.buttonDivision);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculadora";
            this.Load += new System.EventHandler(this.onLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button buttonDivision;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button buttonVezes;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button buttonMenos;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button buttonPonto;
        private System.Windows.Forms.Button buttonMais;
        private System.Windows.Forms.Button buttonIgual;
        private System.Windows.Forms.Button buttonMemoClear;
        private System.Windows.Forms.Button buttonMemoRecover;
        private System.Windows.Forms.Button buttonPotencia;
        private System.Windows.Forms.Button buttonMemoAdd;
        private System.Windows.Forms.Button buttonSqrt;
        private System.Windows.Forms.Button buttonMemoDel;
        private System.Windows.Forms.Button buttonPercent;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonErase;
        private System.Windows.Forms.Button buttonBackspace;
        private System.Windows.Forms.Button buttonToggle;
        private System.Windows.Forms.TextBox textBoxExpression;
        private System.Windows.Forms.TextBox textBoxResult;
    }
}

