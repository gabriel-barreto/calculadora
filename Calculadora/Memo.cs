﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculadora
{
    class Memo
    {
        private double memo = 0;

        public void setMemo(double memo) {
            this.memo = memo;
        }

        public void addMemo(double x) {
            this.setMemo(this.getMemo() + x);
        }

        public void subMemo(double x) {
            this.setMemo(this.getMemo() - x);
        }

        public double getMemo() {
            return this.memo;
        }
    }
}
