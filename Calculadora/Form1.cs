﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Memo Memo = new Memo();
        bool on = false;
        bool op = false;
        bool textBoxFocus = false;
        String lastNumber = "";
        String[] operations = new String[] { "+", "-", "x", "√", "^", "˟", "y", "y˟", "÷" };

        public void onLoad(object sender, EventArgs e) {
            textBoxExpression.Clear();
            textBoxResult.Text = "0";
            on = false;
            lastNumber = "";
            textBoxExpression.ReadOnly = true;
            textBoxResult.ReadOnly = true;
        }

        public void toggleStatus(object sender, EventArgs e) {
            on = !on;
            if (on)
            {
                buttonToggle.Text = "Off";
                buttonToggle.BackColor = Color.FromArgb(231, 76, 60);
                btn0.Enabled = true;
                btn1.Enabled = true;
                btn2.Enabled = true;
                btn3.Enabled = true;
                btn4.Enabled = true;
                btn5.Enabled = true;
                btn6.Enabled = true;
                btn7.Enabled = true;
                btn8.Enabled = true;
                btn9.Enabled = true;
                buttonBackspace.Enabled = true;
                buttonClear.Enabled = true;
                buttonDivision.Enabled = true;
                buttonErase.Enabled = true;
                buttonIgual.Enabled = true;
                buttonMais.Enabled = true;
                buttonMemoClear.Enabled = true;
                buttonMemoDel.Enabled = true;
                buttonMemoAdd.Enabled = true;
                buttonMemoRecover.Enabled = true;
                buttonMenos.Enabled = true;
                buttonPercent.Enabled = true;
                buttonPonto.Enabled = true;
                buttonPotencia.Enabled = true;
                buttonSqrt.Enabled = true;
                buttonVezes.Enabled = true;
            }
            else {
                if (MessageBox.Show("Deseja realmente encerrar?", "Calculadora - Encerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Application.Exit();
                }
                else {
                    on = !on;
                }
            }
        }

        public void hoverTextBoxStatus(object sender, EventArgs e) {
            textBoxResult.Enabled = false;
            textBoxExpression.Enabled = false;
        }

        public void leaveTextBoxStatus(object sender, EventArgs e)
        {
            textBoxResult.Enabled = true;
            textBoxExpression.Enabled = true;
        }

        public void expressionCompute(object sender, EventArgs e)
        {
            String expression = textBoxExpression.Text;
            String[] elementsExpression = expression.Split(' ');
            double result = 0;
            if (elementsExpression.Length == 3) {
                double x = Convert.ToDouble(elementsExpression[0]);
                double y = Convert.ToDouble(elementsExpression[2]);
                switch (elementsExpression[1])
                {
                    case "+":
                        result = (x + y);
                        break;
                    case "-":
                        result = (x - y);
                        break;
                    case "x":
                        result = (x * y);
                        break;
                    case "÷":
                        if (y == 0)
                        {
                            MessageBox.Show("Error", "Syntax Error");
                            clearText(null, null);
                        }
                        else
                        {
                            result = (x / y);
                        }
                        break;
                    case "^":
                        result = Math.Pow(x, y);
                        break;
                    case "√":
                        result = (x * (Math.Sqrt(y)));
                        break;
                    }
            }
            else if (elementsExpression.Length == 2) {
                double x = Convert.ToDouble(elementsExpression[1]);
                switch(elementsExpression[0]) {
                    case "√":
                        result = Math.Sqrt(x);
                        break;
                }
            }
            else if (elementsExpression.Length == 4)
            {
                double x = Convert.ToDouble(elementsExpression[0]);
                double y = Convert.ToDouble(elementsExpression[2]);
                y = (x * (y / 100));
                switch (elementsExpression[1]) {
                    case "+":
                        result = (x + y);
                        break;
                    case "-":
                        result = (x - y);
                        break;
                    case "x":
                        result = (x * y);
                        break;
                    case "÷":
                        if (y == 0)
                        {
                            MessageBox.Show("Error", "Syntax Error");
                            clearText(null, null);
                        }
                        else
                        {
                            result = (x / y);
                        }
                        break;
                    case "^":
                        result = Math.Pow(x, y);
                        break;
                }
            }
            textBoxResult.Text = result.ToString();
            op = false;
            lastNumber = "";
        }


        public void addText(object sender, EventArgs e) {
            Button btnActive = (Button)sender;
            textBoxExpression.Text += btnActive.Text;
            lastNumber += btnActive.Text;
            if (textBoxResult.Text.Length > 0 && textBoxResult.Text != "0") {
                textBoxExpression.Text = textBoxResult.Text;
                textBoxResult.Text = "0";
            }
        }

        public void addOperation(object sender, EventArgs e)
        {
            Button btnActive = (Button)sender;
            if (textBoxResult.Text.Length > 0 && textBoxResult.Text != "0")
            {
                textBoxExpression.Text = textBoxResult.Text;
                textBoxResult.Text = "0";
            }
            if (!op) {
                if (btnActive.Text == "y˟")
                {
                    textBoxExpression.Text += " ^ ";
                }
                else if (btnActive.Text == "√") {
                    textBoxExpression.Text += (textBoxExpression.Text.Length > 0) ? " √ " : "√ "; 
                }
                else
                {
                    textBoxExpression.Text += " " + btnActive.Text + " ";
                }
            }
            else { 
                expressionCompute(null, null);
                if (textBoxResult.Text.Length > 0 && textBoxResult.Text != "0")
                {
                    addOperation(sender, e);
                }
            }
            op = true;
            lastNumber = "";
        }

        public void addPercentage(object sender, EventArgs e) {
            if (!op)
            {
                textBoxResult.Text = (Convert.ToDouble(textBoxExpression.Text) / 100).ToString();
            }
            else {
                textBoxExpression.Text += " %";
                expressionCompute(null, null);
            }
        }

        public void clearText(object sender, EventArgs e) {
            textBoxExpression.Clear();
            textBoxResult.Text = "0";
            lastNumber = "";
            op = false;
        }

        public void clearLastNumber(object sender, EventArgs e) {
            if (lastNumber.Length > 0) {
                textBoxExpression.Text = textBoxExpression.Text.Substring(0, (textBoxExpression.TextLength - lastNumber.Length));
                lastNumber = "";
            }
        }

        public void backspace(object sender, EventArgs e) {
            if (textBoxExpression.Text.Length > 0) {
                bool delOperation = false;
                for (int i = 0; i < operations.Length; i += 1) {
                    if (textBoxExpression.Text.EndsWith(operations[i]))
                    {
                        op = false;
                        delOperation = true;
                    }
                }
                
                textBoxExpression.Text = textBoxExpression.Text.Substring(0, textBoxExpression.Text.Length - 1);

                if (delOperation) {
                    textBoxExpression.Text = textBoxExpression.Text.Substring(0, textBoxExpression.Text.Length - 1);
                }
            }
        }

        public void memoClear(object sender, EventArgs e) {
            Memo.setMemo(0);
        }

        public void memoReturn(object sender, EventArgs e) {
            textBoxExpression.Text = Memo.getMemo().ToString();
        }

        public void memoAdd(object sender, EventArgs e) {
            if (!op)
            {
                if (textBoxResult.Text.Length > 0 && textBoxResult.Text != "0")
                {
                    textBoxExpression.Text = textBoxResult.Text;
                    textBoxResult.Text = "0";
                }
                Memo.addMemo(Convert.ToDouble(textBoxExpression.Text));
            }
            else {
                expressionCompute(null, null);
                Memo.addMemo(Convert.ToDouble(textBoxResult.Text));
            }
        }

        public void memoSub(object sender, EventArgs e)
        {
            if (!op)
            {
                if (textBoxResult.Text.Length > 0 && textBoxResult.Text != "0")
                {
                    textBoxExpression.Text = textBoxResult.Text;
                    textBoxResult.Text = "0";
                }
                Memo.subMemo(Convert.ToDouble(textBoxExpression.Text));
            }
            else
            {
                expressionCompute(null, null);
                Memo.subMemo(Convert.ToDouble(textBoxResult.Text));
            }
        }
    }
}

